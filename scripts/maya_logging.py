from maya import cmds
import os
from datetime import datetime
import ast


class MayaFileLog:
    """ This class is used for logging specific information in file specific logs using a
    dictionary style format.
    """
    def __init__(self):
        """ The last changed dict variable manages the output to the log
        in the form of a dictionary. Essentially the output of the log

        self.pc and self.user gets the computer/user name
        from the OS. It's not completely fail-proof since the name can
        be changed in windows, but when I set this up it was safe to
        assume that it wouldn't change.

        The self.date_time and self.neat_date_time just get the time and date but
        in different formats.

        The self.file queries the file name & path from Maya directly.

        self.log_file will be replaced by using a fucntion. It's the
        name of the log_file which is related to the scene.

        The file_version will be replaced by a function as well. Default
        is v001.
        """
        self.last_changed_dict = {
            "action": "save",
            "datetime": "20200101000000",
            "neat_datetime": "2020/01/01 00:00:00",
            "user": "default.user",
            "pc": "MM0000000",
            "file_version": "v001"
        }

        self.pc = os.environ['COMPUTERNAME']
        self.user = 'user'
        try:
            self.user = os.environ['USER']
        except:
            self.user = os.environ['USERPROFILE'].split('\\')[-1]
        self.neat_date_time = datetime.now().strftime("%H:%M:%S %Y/%m/%d")
        self.date_time = datetime.now().strftime("%Y%m%d%H%M%S")
        self.pc_user = [self.pc, self.user]
        self.file = cmds.file(q=True, l=True)[0]
        self.log_file = ''
        self.file_version = 'v001'

    @staticmethod
    def tail(f, lines=20):
        """ This functions gets the last few lines of a file based on
        the integer used in the lines variable.
        :param f: python fileObject or filepath
        :param lines: The amount of lines to return in a list.
        :return: List of the last amount of lines that will be returned.
        """
        # Try to open the file, if the file doesn't exist for some reason it can't open in read.
        # So instead open with an append flag, close and open in read again for the rest of the
        # function to work.
        try:
            f = open(f, 'a')
            f.close()
            f = open(f, 'r')
        except:
            #print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            #print f
            #print f.name
            f = open(f.name, 'r')
            #print f
            #print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

        total_lines_wanted = lines

        BLOCK_SIZE = 1024
        f.seek(0, 2)
        block_end_byte = f.tell()
        lines_to_go = total_lines_wanted
        block_number = -1
        blocks = []  # blocks of size BLOCK_SIZE, in reverse order starting
        # from the end of the file
        while lines_to_go > 0 and block_end_byte > 0:
            if block_end_byte - BLOCK_SIZE > 0:
                # read the last block we haven't yet read
                f.seek(block_number * BLOCK_SIZE, 2)
                blocks.append(f.read(BLOCK_SIZE))
            else:
                # file too small, start from begining
                f.seek(0, 0)
                # only read what was not read
                blocks.append(f.read(block_end_byte))
            lines_found = blocks[-1].count('\n')
            lines_to_go -= lines_found
            block_end_byte -= BLOCK_SIZE
            block_number -= 1
        all_read_text = ''.join(reversed(blocks))
        f.close()
        dict_list = []
        for line in all_read_text.splitlines()[-total_lines_wanted:]:
            dict_list.append(ast.literal_eval(line))
        return dict_list

    def update_dict(self):
        """ Function to update the last_changed_dict which is used for
        output.
        """
        self.last_changed_dict['file_version'] = self.file_version
        self.last_changed_dict['datetime'] = self.date_time
        self.last_changed_dict['neat_datetime'] = self.neat_date_time
        self.last_changed_dict['user'] = self.user
        self.last_changed_dict['pc'] = self.pc

    def get_metadata_folder(self, post_folder='03_post', metadata='00_metadata'):
        """ Get the project metadata folder based on the input of the
        post  folder and the metadata folder variables.
        :param post_folder: Name of the post folder
        :param metadata: Name of the metadata folder
        :return: Path to the folder.
        """
        maya_file = self.file
        # Separate the folders from each other using the delimiter. The delimiter may be assumed
        # because the output of the maya file query will be consistent.
        folders = maya_file.split('/')
        # Enumerate the folders to get an item from the folders list using the index.
        for i in enumerate(folders):
            # Check if the post folder can be found.
            if i[1] == post_folder:
                basepath = ""
                for num in range(i[0] + 1):
                    basepath += folders[num] + '/'

                # If it's found check if the metadata folder exists.
                if os.path.exists(basepath + metadata):
                    return basepath + metadata
                # If not. try to create the metadata folder.
                else:
                    os.mkdir(basepath + metadata)
                    return basepath + metadata

    def create_log_file(self):
        """ Handles the creation of the actual log file. Uses the get
        metadata folder function.
        :return:
        """
        # Get metadata folder
        dir_metadata = self.get_metadata_folder() + '/'
        # Get filename and split trim the extension and version
        filesplit = self.file.split('/')[-1].split('.')[0].split('_')
        # Get the file version and convert to ascii
        self.file_version = filesplit[-1].encode('ascii')
        # Create the filename by joining the entire list together except the last entry.
        filename = ''.join(filesplit[0:-1])
        log_filename = filename + '_log.txt'
        if os.path.exists(dir_metadata + log_filename):
            self.log_file = dir_metadata + log_filename
            return dir_metadata + log_filename

        log_file = ''
        # Try to create the log file
        try:
            log_file = open(dir_metadata+'/'+filename+'_log.txt', "a")
            log_file.close()
        # If it can't create the logfile return the path
        except:
            log_file = dir_metadata + log_filename

        self.log_file = log_file
        return log_file

    def log_file_action(self, action='save'):
        """ This function logs the action input in the action variable.
        It simply adds the string to the action key in the dict.
        :param action: Name of the action performed.
        """
        # Update dictionary
        self.update_dict()
        opened_dict = self.last_changed_dict
        # Modify the action key with the value
        opened_dict['action'] = action
        log_file = open(str(self.log_file), "a")
        log_file.write(str(opened_dict)+'\n')
        log_file.close()

    def get_log_dicts(self):
        """ Gets the dictionaries listed in the log.
        :return: Last 10 lines that contain dictionaries.
        """
        return self.tail(f=self.log_file, lines=10)

    def compare_log(self):
        """ This function compares the current user to what's listed in
        the logs on multiple occasions. If the listed user is not the
        same as the logged user it will output either True or False.
        :return: True or False depending on the situation. In one
        condition might output a list of the latest dictionary and a
        False statement.
        """
        # Get the last entry in the file
        latest_entry = None
        if len(self.get_log_dicts()) > 0:
            latest_entry = self.get_log_dicts()[-1]
        else:
            return True
        # Check action and user for the current user
        if latest_entry['action'] != 'close' and latest_entry['user'] != self.user:
            x = -1
            while latest_entry['action'] != 'open':
                latest_entry = self.get_log_dicts()[x]
                x -= 1

            if latest_entry['user'] != self.user:
                return False, latest_entry
            else:
                return True
        # If the latest entry is close but it was not by the current user, check if the last save
        # was by the current user.
        elif latest_entry['action'] == 'close' and latest_entry['user'] != self.user:
            x = -1
            while latest_entry['action'] != 'save':
                latest_entry = self.get_log_dicts()[x]
                x -= 1

            if latest_entry['user'] != self.user:
                return False, latest_entry
            else:
                return True
        else:
            return True

    def read_write_usage(self):
        pass

    def write_file_usage(self):
        pass
