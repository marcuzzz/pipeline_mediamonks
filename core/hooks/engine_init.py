# Copyright (c) 2018 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

"""
Hook that gets executed every time an engine has been fully initialized.
"""

from tank import Hook

import sys
import platform

if platform.system() == 'Darwin':
    sys.path.append('/Volumes/postdata/_RESOURCES/SCRIPTS/shotgun')
else:
    sys.path.append('M:/_RESOURCES/SCRIPTS/shotgun')

class EngineInit(Hook):

    def execute(self, engine, **kwargs):
        """
        Executed when a Toolkit engine has been fully initialized.

        At this point, all apps and frameworks have been loaded,
        and the engine is fully operational.

        The default implementation does nothing.

        :param engine: Engine that has been initialized.
        :type engine: :class:`~sgtk.platform.Engine`
        """
        if engine.name == "tk-maya":
            # Import Maya related modules after initializing otherwise the init will fail.
            import maya_logging as maya_log
            reload(maya_log)
            import maya.api.OpenMaya as om
            from maya import cmds
            import sgtk
            # Create class
            mfl = maya_log.MayaFileLog()
            # Overwrite file, sometimes it fails on class init.
            mfl.file = cmds.file(q=True, l=True)[0]
            # If the callbacks somehow already exist, remove them
            try:
                om.MSceneMessage.removeCallback(cb_save)
                om.MSceneMessage.removeCallback(cb_open)
                om.MSceneMessage.removeCallback(cb_close)
            except:
                pass

            # This function implements a confirm dialog window which will give you the options to
            # Cancel the save operation with a check.
            def before_save_check(*args, **kwargs):
                mfl = maya_log.MayaFileLog()
                mfl.file = cmds.file(q=True, l=True)[0]
                mfl.create_log_file()
                if mfl.compare_log() is True:
                    mfl.log_file_action('save')
                    return True
                message = 'This scene ({2}) is currently opened or saved on {0} ({1}), please notify them before saving'.format(mfl.compare_log()[1]['pc'], mfl.compare_log()[1]['user'], mfl.compare_log()[1]['file_version'])
                options = ['Save', 'Cancel']
                confirm = cmds.confirmDialog(title='Save Warning', 
                                            m=message,
                                            button=options, 
                                            db=options[0], 
                                            cb=options[1], 
                                            ds=options[1])

                # This overwrites the callback abort message.
                if confirm == options[1]:
                    string_key = "s_TfileIOStrings.rFileOpCancelledByUser"
                    string_error = 'Cancelled Save Operation'
                    cmds.displayString(string_key, replace=True, value=string_error)
                    return False
                else:
                    mfl.log_file_action('save')
                    return True

            # This check is a little simpler than the save check and simply gives a promt to
            # Confirm that the file is also open on another computer.
            def before_open_check(*args, **kwargs):
                mfl = maya_log.MayaFileLog()
                mfl.file = cmds.file(q=True, l=True)[0]
                mfl.create_log_file()
                if mfl.compare_log() is True:
                    mfl.log_file_action('open')
                    return
                message = 'This scene ({2}) is currently opened on {0} ({1}), please check before making any changes.'.format(mfl.compare_log()[1]['pc'], mfl.compare_log()[1]['user'], mfl.compare_log()[1]['file_version'])
                confirm = cmds.confirmDialog(title='Open Warning', m=message)
                mfl.log_file_action('open')
                
            def before_close_check(*args, **kwargs):
                mfl = maya_log.MayaFileLog()
                mfl.file = cmds.file(q=True, l=True)[0]
                mfl.create_log_file()
                mfl.log_file_action('close')

            cb_save = om.MSceneMessage.addCheckCallback(33, before_save_check)
            cb_open = om.MSceneMessage.addCallback(8, before_open_check)
            cb_close = om.MSceneMessage.addCallback(30, before_close_check)



        if engine.name == "tk-nuke":
            from PySide2 import QtWidgets
            import nuke
            import nuke_logging

            nfl = nuke_logging.nukeFileLog()

            def before_open_check():
                nfl.create_log_file()

                if nfl.compare_log()[0] is False:
                    message = 'This scene ({2}) is currently opened on {0} ({1}), please check before making any changes.'.format(
                        nfl.compare_log()[1]['pc'], nfl.compare_log()[1]['user'],
                        nfl.compare_log()[1]['file_version'])

                    msg_box = QtWidgets.QMessageBox()
                    msg_box.setText(message)
                    msg_box.exec_()
                    nfl.log_file_action('open')

                else:
                    nfl.log_file_action('open')
                    print ('Opened Successfully')

            def before_save_check():
                nfl.create_log_file()

                nfl.create_log_file()

                if nfl.compare_log()[0] is False:
                    message = 'This scene ({2}) is currently saved or opened on {0} ({1}), please check before making any changes.'.format(
                        nfl.compare_log()[1]['pc'], nfl.compare_log()[1]['user'],
                        nfl.compare_log()[1]['file_version'])

                    msg_box = QtWidgets.QMessageBox()
                    msg_box.setText(message)
                    msg_box.setStandardButtons(
                        QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
                    resp = msg_box.exec_()
                    if resp == QtWidgets.QMessageBox.Ok:
                        nfl.log_file_action('save')
                    else:
                        raise Exception('Save Cancelled')

                else:
                    nfl.log_file_action('open')
                    print ('Saved Successfully')


            def before_close_check():
                nfl.create_log_file()
                nfl.log_file_action('close')

            nuke.addOnScriptLoad(before_open_check)
            nuke.addOnScriptSave(before_save_check)
            nuke.addOnScriptClose(before_close_check)
